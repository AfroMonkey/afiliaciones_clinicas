from odoo import api, fields, models, _


class Procedure(models.Model):
    _name = 'procedure'
    _description = _('Procedure to practice in a patient')

    name = fields.Char(
        required=True,
    )
    code = fields.Char(
        required=True,        
        index=True,
    )
    affiliation_ids = fields.Many2many(
        comodel_name='affiliation', 
        string=_('Affiliations'),
    )
    affilaitions_count = fields.Integer(
        compute='_compute_affiliations_count',
        store=True,
    )

    @api.depends('affiliation_ids')
    def _compute_affiliations_count(self):
        for r in self:
            r.affilaitions_count = len(r.affiliation_ids)


    _sql_constraints = [
        (
            'name_unique',
            'UNIQUE(name)',
            _('The name of procedure must be unique.')
        ),
        (
            'code_unique',
            'UNIQUE(code)',
            _('The code of procedure must be unique.')
        )
    ]
    
