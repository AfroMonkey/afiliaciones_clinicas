import urllib.request

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


def get_names_by_dni(dni):
    url = 'http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI={dni}'.format(dni=dni)
    with urllib.request.urlopen(url) as response:
        data = response.read().decode('utf-8').split('|')
        print(data)
        if len(data) <= 3:
            return {
                'last_name': data[0],
                'mother_last_name': data[1],
                'first_name': data[2],
            }
    return False

class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_doctor = fields.Boolean()
    cmp = fields.Integer(
        index=True,
        string=_('CMP'),
    )

    is_patient = fields.Boolean()
    clinic_history = fields.Char(
        index=True,
    )
    relative_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Relative'),
    )
    birth_day = fields.Date()
    age = fields.Integer(compute='_get_age')
    nationality_id = fields.Many2one(
        comodel_name='res.country',
        string=_('Nationality'),
    )

    @api.depends('birth_day')
    def _get_age(self):
        for r in self:
            if r.birth_day:
                r.age = (fields.Date.today() - r.birth_day).days / 365

    @api.constrains('clinic_history')
    def _check_clinic_history(self):
        for r in self:
            if r.clinic_history and self.search_count([('clinic_history', '=', r.clinic_history)]) > 1:
                raise ValidationError(_('Clinic History must be uniques.'))

    @api.constrains('vat')
    def _check_vat(self):
        for r in self:
            if r.vat and self.search_count([('vat', '=', r.vat)]) > 1:
                raise ValidationError(_('DNI must be unique.'))

    @api.constrains('cmp')
    def _check_cmp(self):
        for r in self:
            if r.cmp and self.search_count([('cmp', '=', r.cmp)]) > 1:
                raise ValidationError(_('CMP must be unique.'))

    @api.multi
    def get_next_clinic_history(self):
        self.ensure_one()
        if self.clinic_history:
            raise ValidationError(_('This patient already have an Clinic History.'))
        self.clinic_history = self.env['ir.sequence'].next_by_code('clinic_history_sequence')

    @api.one
    def update_document(self):  # Overwrite from odoope_ruc_validation
        if not self.vat:
            return
        if self.catalog_06_id and self.catalog_06_id.code == '1':
            if len(self.vat) != 8:
                raise Warning('El Dni debe tener 8 caracteres')
            else:
                data = get_names_by_dni(self.vat)
                if data:
                    self.firstname = data['first_name']
                    self.lastname = "{} {}".format(data['last_name'], data['mother_last_name'])


            
