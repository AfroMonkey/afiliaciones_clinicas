from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import datetime

class Affiliation(models.Model):
    _name = 'affiliation'
    _description = _('Relation betwen patients and procedures')

    name = fields.Integer(
        related='id',
    )
    entry_time = fields.Datetime(
        default=fields.Datetime.now,
    )
    month = fields.Integer(
        compute='_get_month',
    )
    clinic_history = fields.Char(
        compute='_get_clinic_history',
        inverse='_set_patient_id',
        store=True,
    )
    patient_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Patient'),
        required=True,
    )
    patient_lastname = fields.Char(
        related='patient_id.lastname',
    )
    patient_firstname = fields.Char(
        related='patient_id.firstname',
    )
    patient_street = fields.Char(
        related='patient_id.street',
    )
    patient_phone = fields.Char(
        related='patient_id.phone',
        string=_('Patient phone'),
    )
    patient_province_id = fields.Many2one(
        related='patient_id.province_id',
    )
    patient_relative_id = fields.Many2one(
        related='patient_id.relative_id',
    )
    patient_relative_phone = fields.Char(
        related='patient_relative_id.phone',
        string=_('Relative phone'),
    )
    patient_vat = fields.Char(
        related='patient_id.vat',
        string=_('DNI'),
    )
    patient_age = fields.Integer(
        related='patient_id.age',
    )
    patient_email = fields.Char(
        related='patient_id.email',
    )
    patient_nationality_id = fields.Many2one(
        related='patient_id.nationality_id',
    )
    cmp = fields.Integer(
        compute='_get_cmp',
        inverse='_set_doctor_id',
        string=_('CMP'),
    )
    doctor_id = fields.Many2one(
        comodel_name='res.partner',
        domain=[('is_doctor', '=', True)],
        string=_('Doctor'),
        required=True,        
    )
    doctor_phone = fields.Char(
        related='doctor_id.phone',
    )
    bracelet_no = fields.Integer(
    )
    status = fields.Selection([
            ('in', 'INGRESO'),
            ('out', 'POSTERGADO'),
            ('re', 'REINGRESO'),
        ],
    )
    procedure_ids = fields.Many2many(
        comodel_name='procedure',
        required=True,
    )
    sop_entry_time = fields.Datetime(
        string=_('SOP Entry Time'),
        required=True,
    )
    sop_exit_time = fields.Datetime(
        string=_('SOP Exit Time'),
    )
    surgery_time = fields.Datetime(
    )
    have_lab = fields.Boolean()
    have_card = fields.Boolean()
    have_both = fields.Boolean()
    need_lab = fields.Boolean()
    need_card = fields.Boolean()
    need_both = fields.Boolean()
    additional_analysis = fields.Text()
    currency_id = fields.Many2one(
        comodel_name='res.currency',
        default=lambda self: self.env.ref('base.main_company').currency_id.id
    )
    doctor_payment = fields.Monetary(
        currency_field='currency_id',
    )
    patient_payment = fields.Monetary(
        currency_field='currency_id',
    )
    state = fields.Selection(
        selection=[
            ('appointment', 'Appointment'),
            ('affiliation', 'Affiliation'),
        ],
        default='appointment',
    )
    hall_id = fields.Many2one(
        comodel_name='hall',
        string=_('Hall'),
        index=True,
        required=True,
    )
    tower = fields.Integer(
        index=True,
    )
    anesthesiologist_id = fields.Many2one(
        comodel_name='res.partner',
        string=_('Anesthesiologist'),
        index=True,
    )
    planned_date = fields.Date(
        default=fields.Date.today,
    )
    

    @api.depends('patient_id')
    def _get_clinic_history(self):
        for r in self:
            if r.patient_id:
                r.clinic_history = r.patient_id.clinic_history
    
    @api.onchange('clinic_history')
    def _set_patient_id(self):
        for r in self:
            if r.clinic_history:
                r.patient_id = self.env['res.partner'].search([('clinic_history', '=', r.clinic_history)], limit=1)

    @api.depends('doctor_id')
    def _get_cmp(self):
        for r in self:
            if r.doctor_id:
                r.cmp = r.doctor_id.cmp

    @api.onchange('cmp')
    def _set_doctor_id(self):
        if self.cmp:
            self.doctor_id = self.env['res.partner'].search([('cmp', '=', self.cmp)], limit=1)

    @api.depends('entry_time')
    def _get_month(self):
        for r in self:
            if r.entry_time:
                r.month = r.entry_time.month
            
    @api.constrains('clinic_history')
    def _check_clinic_history(self):
        for r in self:
            if r.clinic_history and not self.env['res.partner'].search_count([('clinic_history', '=', r.clinic_history)]):
                raise ValidationError(_('That Clinic History does not exists'))


    @api.constrains('cmp')
    def _check_cmp(self):
        for r in self:
            if r.cmp and not self.env['res.partner'].search_count([('cmp', '=', r.cmp)]):
                raise ValidationError(_('That CMP does not exists'))

    _sql_constraints = [
        (
            'positive_doctor_payment',
            'CHECK(doctor_payment >= 0)',
            _('Doctor payment can not be negative.')
        ),
        (
            'positive_patient_payment',
            'CHECK(patient_payment >= 0)',
            _('Patient payment can not be nebracelet_sequencegative.')
        ),
        (
            'bracelet_no_unique',
            'UNIQUE(bracelet_no)',
            _('Bracelet No must be unique.')
        ),
    ]

    @api.onchange('have_lab')
    def _onchange_have_lab(self):
        if self.have_lab:
            self.need_lab = False
            self.need_both = False

    @api.onchange('have_card')
    def _onchange_have_card(self):
        if self.have_card:
            self.need_card = False
            self.need_both = False

    @api.onchange('need_lab')
    def _onchange_need_lab(self):
        if self.need_lab:
            self.have_lab = False
            self.have_both = False

    @api.onchange('need_card')
    def _onchange_need_card(self):
        if self.need_card:
            self.have_card = False
            self.have_both = False

    @api.onchange('have_both')
    def _onchange_have_both(self):
        if self.have_both:
            self.have_lab = False
            self.have_card = False
            self.need_lab = False
            self.need_card = False
            self.need_both = False

    @api.onchange('need_both')
    def _onchange_need_both(self):
        if self.need_both:
            self.have_lab = False
            self.have_card = False
            self.need_lab = False
            self.need_card = False
            self.have_both = False

    @api.multi
    def get_next_bracelet_no(self):
        self.ensure_one()
        if self.bracelet_no:
            raise ValidationError(_('This affiliation already have an Bracelet No'))
        self.bracelet_no = self.env['ir.sequence'].next_by_code('bracelet_sequence')

    # TODO check patient info
