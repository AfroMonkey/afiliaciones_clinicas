Afiliaciones Clinicas
=====================

Desarrollo-odoo
---------------

Modulo para el registro de afiliaciones en una clinica.

TODO's
------

1. [x] 10. Volver HCL opcional y char
2. [x] 5 . Mensaje de alerta si no existe affiliation.cmp
3. [x] 5 . Crear patitent desde affiliation
4. [x] 5 . Crear doctor desde affiliation
5. [x] 20 . Traducciones
6. [x] 7 . Comprobar formato de affiliation.surgery_time & sop en 24hrs
7. [x] 5 . Opciones excluyentes en affiliation.pre-surgery
8. [x] 5 . Evitar pagas negativas
9. [x] 10 . Colocar validacion de Unicidad en patient.clinic_history, affiliation.bracelet_no, res.partner.dni, res.partner.cmp
10. [x] 10 . Habilitar busquedas por defecto mediante: patient, doctor, procedure, entry_time, id, dni, cmp
11. [x] 10 . Añadir agrupadores mediante: patient, doctor, procedure, entry_time
12. [x] 15 . Añadir vista de gráfica para procedures, colocar código en el mismo
13. [x] 10 . Añadir vista de gráfica para affiliation, añadir el state
14. [x] 5 . Hacer obligatorio res.partner.phone para doctores
15. [x] 10 . Mejorar visualmente el formulario res.partner
16. [ ] 15 . Hacer que en la pestaña patient & doctor se creen lo registros con el boolean
17. [x] 5 . Comprobar warnings
18. [x] 5 . Cambiar icono
19. [x] API DNI
20. [x] X en lugar de True en PDF
21. [x] Añadir vista de lista para salas
22. [x] Remover campos olbigatorios en BD y colocarlo en la vista
23. [x] Vista de calendario para salas
1. [x] Quitar campo affiliation.name
1. [ ] Renombrar los campos de analisis
